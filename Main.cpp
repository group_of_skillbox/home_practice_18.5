#include<iostream>
#include<string>
#include<Windows.h>

using namespace std;

class Human
{
public:
	void SetName(string Name)
	{
		PlayerName = Name;
	}
	void SetScore(int Score)
	{
		PlayerScore = Score;
	}

	string GetName()
	{
		return PlayerName;
	}

	int GetScore()
	{
		return PlayerScore;
	}

private:
	int PlayerScore{};
	string PlayerName;
};

int main()
{
	setlocale(LC_ALL, "ru");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	Human Player;
	string Name;
	int Score{};

	bool Status = true;
	int Volume{};
	int Count{};

	cout << "������� ���������� �������: ";
	cin >> Volume;
	cout << endl;

	int* arr = new int[Volume];
	string* NamesOfPlayer = new string[Volume];

	for (int i = 0; i < Volume; i++)
	{
		cout << "������� ��� ������: ";
		cin >> Name;
		Player.SetName(Name);
		NamesOfPlayer[i] = Player.GetName();

		cout << "������� ���� ��� ������ " << NamesOfPlayer[i] << ": ";
		cin >> Score;
		Player.SetScore(Score);
		arr[i] = Player.GetScore();
		Count++;

	}
	cout << "\n\n";

	while (Status)
	{
		Status = false;
		for (int i = 0; i < Volume; i++)
		{
			if (arr[i] < arr[i + 1])
			{
				swap(arr[i], arr[i + 1]);
				swap(NamesOfPlayer[i], NamesOfPlayer[i + 1]);
				Status = true;
			}
		}
		Volume--;
	}

	cout << "��������������� ������ (Bubble sort):" << "\n\n";
	for (int i = 0; i < Count; i++)
	{
		cout << NamesOfPlayer[i] << "\t" << arr[i] << endl;
	}

	delete[] arr;
	delete[] NamesOfPlayer;
}